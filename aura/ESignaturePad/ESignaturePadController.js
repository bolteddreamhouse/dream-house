({
    Init : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    clear:function(component, event, helper){
        helper.clearHelper(component, event, helper);
    },
    save:function(component, event, helper){
        helper.saveHelper(component, event, helper);
    }
})