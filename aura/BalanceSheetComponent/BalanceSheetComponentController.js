({
    loadFinanceData : function(component, event, helper) {
        //var accId = "0010o00002JHElgAAH";
        var accId = component.get("v.recordId");
        var assets = component.get("c.getAssets");
        assets.setParams({'accountId' : accId});
        assets.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var years = Object.keys(response.getReturnValue());
                var options=[];
                years.forEach(function(year) {
                    options.push({ "value": year, "label": year });
                });
                component.set("v.options", options);
                component.set("v.yearAssetMap",response.getReturnValue());
                console.log("yearAssetMap:" + JSON.stringify(response.getReturnValue()));
                 //var accId = "0010o00002JHElgAAH";
                var liability = component.get("c.getLiablities");
                liability.setParams({'accountId' : accId});
                liability.setCallback(this,function(response){
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        component.set("v.yearLiabilityMap",response.getReturnValue());
                        console.log("yearLiabilityMap:" + JSON.stringify(response.getReturnValue()));
                        var yearAssetMap = component.get("v.yearAssetMap");
        				var yearLiabilityMap = component.get("v.yearLiabilityMap");
                        component.set("v.assetId", yearAssetMap[2018]);
       					component.set("v.liabilityId", yearLiabilityMap[2018]);
                    }
                    else{	console.log("Error Occured" + state);	}
                });
                 $A.enqueueAction(liability);
            }
            else{	console.log("Error Occured" + state);	}
        });
        
         
        $A.enqueueAction(assets);
       
    },
    
    handleChange : function(component, event, helper){
        var selectedOptionValue = event.getParam("value");
        var yearAssetMap = component.get("v.yearAssetMap");
        var yearLiabilityMap = component.get("v.yearLiabilityMap");
        component.set("v.assetId", yearAssetMap[selectedOptionValue]);
        component.set("v.liabilityId", yearLiabilityMap[selectedOptionValue]);
    }
})